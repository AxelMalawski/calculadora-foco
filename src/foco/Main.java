package foco;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Main {

    public static void main(String[] args) {

        MarcoFoco miMarco = new MarcoFoco();
        miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        miMarco.setSize(600, 600);
        
    }
    
}
class MarcoFoco extends JFrame{
    
    public MarcoFoco(){
        
    	
        setVisible(true);    
        setBounds(400, 300, 400, 200);
        
        add(new LaminaFoco());
       
    }
   
}

class LaminaFoco extends JPanel{
            
   public LaminaFoco(){
       
       setLayout(null);
       
       cuadro1 = new JTextField();
       cuadro2 = new JTextField();
       
       cuadro1.setVisible(true);
       cuadro2.setVisible(true);
       
       cuadro1.setBounds(120, 10, 150, 50);
       cuadro2.setBounds(120, 60, 150, 50);
       
       add(cuadro1);
       add(cuadro2);

       FocoListener foco = new FocoListener();
       cuadro1.addFocusListener(foco);
    }
   
   public void paintComponent(Graphics g){
       
       super.paintComponent(g);  
                     
    }
    private JTextField cuadro1;
    private JTextField cuadro2;
    
   private class FocoListener implements FocusListener{

        public void focusGained(FocusEvent e) {
         
        	System.out.println("El elemento ha ganado el foco");
        }
        
        public void focusLost(FocusEvent e) {
            
            System.out.println("El elemento ha perdido el foco");
        }
    
    }
    
}