package calc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculadora implements ActionListener{

	JFrame frame;
	JTextField pantalla;
	JButton[] botonesNumeros = new JButton[10];
	JButton[] botonesFunciones = new JButton[8];
	JButton botonSumar,botonRestar,botonMult, botonDiv;
	JButton botonDecimal, botonIgual, botonBorrar, botonLimpiar;
	JPanel panel;
	
	Font font = new Font("Arial",Font.BOLD,30);
	
	double num1=0,num2=0,resultado=0;
	char operacion;
	
	Calculadora(){
		
		frame = new JFrame("Calculadora Lab II");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(420, 550);
		frame.setLayout(null);
		
		pantalla = new JTextField();
		pantalla.setBounds(50, 25, 300, 50);
		pantalla.setFont(font);
		pantalla.setEditable(false);
		
		botonSumar = new JButton("+");
		botonRestar = new JButton("-");
		botonMult = new JButton("*");
		botonDiv = new JButton("/");
		botonDecimal = new JButton(".");
		botonIgual = new JButton("=");
		botonBorrar = new JButton("Del");
		botonLimpiar = new JButton("Clr");

		
		botonesFunciones[0] = botonSumar;
		botonesFunciones[1] = botonRestar;
		botonesFunciones[2] = botonMult;
		botonesFunciones[3] = botonDiv;
		botonesFunciones[4] = botonDecimal;
		botonesFunciones[5] = botonIgual;
		botonesFunciones[6] = botonBorrar;
		botonesFunciones[7] = botonLimpiar;

		
		for(int i =0;i<8;i++) {
			botonesFunciones[i].addActionListener(this);
			botonesFunciones[i].setFont(font);
			botonesFunciones[i].setFocusable(false);
		}
		
		for(int i =0;i<10;i++) {
			botonesNumeros[i] = new JButton(String.valueOf(i));
			botonesNumeros[i].addActionListener(this);
			botonesNumeros[i].setFont(font);
			botonesNumeros[i].setFocusable(false);
		}
		

		botonBorrar.setBounds(150,430,100,50);
		botonLimpiar.setBounds(250,430,100,50);
		
		panel = new JPanel();
		panel.setBounds(50, 100, 300, 300);
		panel.setLayout(new GridLayout(4,4,10,10));

		panel.add(botonesNumeros[1]);
		panel.add(botonesNumeros[2]);
		panel.add(botonesNumeros[3]);
		panel.add(botonSumar);
		panel.add(botonesNumeros[4]);
		panel.add(botonesNumeros[5]);
		panel.add(botonesNumeros[6]);
		panel.add(botonRestar);
		panel.add(botonesNumeros[7]);
		panel.add(botonesNumeros[8]);
		panel.add(botonesNumeros[9]);
		panel.add(botonMult);
		panel.add(botonDecimal);
		panel.add(botonesNumeros[0]);
		panel.add(botonIgual);
		panel.add(botonDiv);
		
		frame.add(panel);
		frame.add(botonBorrar);
		frame.add(botonLimpiar);
		frame.add(pantalla);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		
		Calculadora calc = new Calculadora();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		for(int i=0;i<10;i++) {
			if(e.getSource() == botonesNumeros[i]) {
				pantalla.setText(pantalla.getText().concat(String.valueOf(i)));
			}
		}
		if(e.getSource()==botonDecimal) {
			pantalla.setText(pantalla.getText().concat("."));
		}
		if(e.getSource()==botonSumar) {
			num1 = Double.parseDouble(pantalla.getText());
			operacion ='+';
			pantalla.setText("");
		}
		if(e.getSource()==botonRestar) {
			num1 = Double.parseDouble(pantalla.getText());
			operacion ='-';
			pantalla.setText("");
		}
		if(e.getSource()==botonMult) {
			num1 = Double.parseDouble(pantalla.getText());
			operacion ='*';
			pantalla.setText("");
		}
		if(e.getSource()==botonDiv) {
			num1 = Double.parseDouble(pantalla.getText());
			operacion ='/';
			pantalla.setText("");
		}
		if(e.getSource()==botonIgual) {
			num2=Double.parseDouble(pantalla.getText());
			
			switch(operacion) {
			case'+':
				resultado=num1+num2;
				break;
			case'-':
				resultado=num1-num2;
				break;
			case'*':
				resultado=num1*num2;
				break;
			case'/':
				resultado=num1/num2;
				break;
			}
			pantalla.setText(String.valueOf(resultado));
			num1=resultado;
		}
		if(e.getSource()==botonLimpiar) {
			pantalla.setText("");
		}
		if(e.getSource()==botonBorrar) {
			String string = pantalla.getText();
			pantalla.setText("");
			for(int i=0;i<string.length()-1;i++) {
				pantalla.setText(pantalla.getText()+string.charAt(i));
			}
		}
	
	}
}